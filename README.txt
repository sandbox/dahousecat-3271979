Add a tab to config > export for multiple.

Add a tab to config > export for entity to show all field config for a specific entity bundle.

Shows a table with every single config item.

Can filter the table to only show items that match a search string.

Can select multiple items from the table and export them to a folder of your choice or download a zip.

Also allows quickly editing each active config item.
