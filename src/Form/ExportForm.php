<?php

namespace Drupal\config_export_multiple\Form;

use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use ZipArchive;

/**
 * Class to build ExportForm for exporting multiple configuration items.
 *
 * @package Drupal\config_export_multiple\Form
 */
class ExportForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The config storage.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected StorageInterface $configStorage;

  /**
   * Tracks the valid config entity type definitions.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface[]
   */
  protected array $definitions = [];

  /**
   * The value to filter by.
   *
   * @var string
   */
  protected string $filter;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * Constructs a new ConfigSingleImportForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Config\StorageInterface $configStorage
   *   The config storage.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, StorageInterface $configStorage, FileSystemInterface $fileSystem) {
    $this->entityTypeManager = $entityTypeManager;
    $this->configStorage     = $configStorage;
    $this->fileSystem        = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ExportForm {

    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');

    /** @var \Drupal\Core\Config\StorageInterface $config_storage */
    $config_storage = $container->get('config.storage');

    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = $container->get('file_system');

    return new static($entity_type_manager, $config_storage, $file_system);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'config-export-multiple';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $query = $this->getRequest()->query;

    $this->filter = $query->get('filter') ?? '';

    $form['filter'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Filter'),
      '#default_value' => $this->filter,
    ];

    $form['search'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Search'),
    ];

    $form['table'] = $this->buildTable($this->configStorage->listAll());

    $form['folder'] = [
      '#type'     => 'textfield',
      '#title'    => $this->t('Export folder'),
      '#required' => FALSE,
    ];

    $form['export'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Export'),
    ];

    $form['download'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Download zip'),
    ];

    return $form;
  }

  /**
   * Build the config table.
   *
   * @param array $config
   *   The config to list.
   *
   * @return array
   *   The config table render array.
   */
  protected function buildTable(array $config): array {

    $header = [
      'name'    => $this->t('Name'),
      'actions' => $this->t('Actions'),
    ];
    $rows   = [];

    foreach ($config as $name) {

      if (isset($this->filter) && $this->filter && strpos($name, $this->filter) === FALSE) {
        continue;
      }

      $edit     = Url::fromRoute('config.edit_single', ['name' => $name]);
      $download = Url::fromRoute('config.download_single', ['name' => $name]);
      $delete   = Url::fromRoute('config.delete_single', ['name' => $name]);

      $rows[$name] = [
        'name'    => $name,
        'actions' => [
          'data' => [
            'edit'     => [
              '#type'  => 'link',
              '#url'   => $edit,
              '#title' => $this->t('Edit'),
            ],
            'download' => [
              '#prefix' => ' | ',
              '#type'   => 'link',
              '#url'    => $download,
              '#title'  => $this->t('Download'),
            ],
            'delete'   => [
              '#prefix' => ' | ',
              '#type'   => 'link',
              '#url'    => $delete,
              '#title'  => $this->t('Delete'),
            ],
          ],
        ],
      ];
    }

    return [
      '#type'    => 'tableselect',
      '#header'  => $header,
      '#options' => $rows,
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    $triggering_element = $form_state->getTriggeringElement();
    $action             = $triggering_element['#parents'][0];
    $folder             = $form_state->getValue('folder');

    if ($action === 'export' && !is_writable($folder)) {
      $args = ['%folder' => $folder];
      $form_state->setErrorByName('folder', $this->t('%folder is not writable', $args));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $triggering_element = $form_state->getTriggeringElement();

    $action = $triggering_element['#parents'][0];

    if ($action === 'search') {
      $url = Url::fromRoute('config.export_multiple')
        ->setOption('query', ['filter' => $form_state->getValue('filter')]);

      $form_state->setRedirectUrl($url);
    }
    elseif ($action === 'export') {
      $configs = array_filter(array_values($form_state->getValue('table')));
      $folder  = $form_state->getValue('folder');
      $this->writeConfigs($configs, $folder);
    }
    elseif ($action === 'download') {
      $configs = array_filter(array_values($form_state->getValue('table')));
      $form_state->setResponse($this->downloadZip($configs));
    }

  }

  /**
   * Write config files to the file system.
   *
   * @param array $configs
   *   An array of config names.
   * @param string $folder
   *   The folder to write the config to.
   */
  protected function writeConfigs(array $configs, string $folder): void {

    foreach ($configs as $name) {
      $config = Yaml::encode($this->configStorage->read($name));

      $filename = rtrim($folder, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $name . '.yml';
      $args     = ['%filename' => $filename, '@user' => exec('whoami')];

      if (!is_writable($filename)) {
        $this->messenger()
          ->addError($this->t('%filename is not writable. I am @user', $args));
        continue;
      }

      $result = file_put_contents($filename, $config);

      if ($result) {
        $this->messenger()->addStatus($this->t('Wrote %filename', $args));
      }
      else {
        $this->messenger()
          ->addError($this->t('Failed to write %filename', $args));
      }

    }

  }

  /**
   * Download a zip of configs.
   *
   * @param array $configs
   *   The config ids.
   * @param string|null $fileName
   *   The download name.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   *   The binary file response.
   */
  protected function downloadZip(array $configs, ?string $fileName = NULL): BinaryFileResponse {

    $fileName = ($fileName ?? 'configs') . '.zip';
    $fileName = str_replace(':', '_', $fileName);

    $zip  = new ZipArchive();
    $path = $this->fileSystem->getTempDirectory() . '/' . $fileName;

    if ($zip->open($path, ZipArchive::CREATE) !== TRUE) {
      throw new \RuntimeException("cannot open $path");
    }

    foreach ($configs as $name) {
      $config = Yaml::encode($this->configStorage->read($name));

      // Remove uuid.
      $lines = explode(PHP_EOL, $config);
      foreach ($lines as $key => $line) {
        if (strpos($line, 'uuid:') === 0) {
          unset($lines[$key]);
        }
      }
      $config = implode(PHP_EOL, $lines);

      $zip->addFromString($name . '.yml', $config);
    }

    $zip->close();

    $headers = [
      'Content-Type'        => 'application/zip',
      'Content-Description' => 'File Download',
      'Content-Disposition' => 'attachment; filename=' . $fileName,
    ];

    // Return and trigger file download.
    return new BinaryFileResponse($path, 200, $headers, TRUE);

  }

}
