<?php

namespace Drupal\config_export_multiple\Form;


use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to show all field config for a bundle.
 *
 * @package Drupal\config_export_multiple\Form
 */
class EntityForm extends ExportForm {

  /**
   * Bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $bundleInfo;

  /**
   * The field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $fieldManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    StorageInterface $configStorage,
    FileSystemInterface $file_system,
    EntityTypeBundleInfoInterface $bundleInfo,
    EntityFieldManagerInterface $fieldManager
  ) {
    parent::__construct($entityTypeManager, $configStorage, $file_system);
    $this->bundleInfo   = $bundleInfo;
    $this->fieldManager = $fieldManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ExportForm {

    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');

    /** @var \Drupal\Core\Config\StorageInterface $config_storage */
    $config_storage = $container->get('config.storage');

    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = $container->get('file_system');

    /** @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info */
    $bundle_info = $container->get('entity_type.bundle.info');

    /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager */
    $field_manager = $container->get('entity_field.manager');

    return new static($entity_type_manager, $config_storage, $file_system, $bundle_info, $field_manager);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'config-export-entity';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $options = [];

    $entities = $this->entityTypeManager->getDefinitions();

    foreach ($entities as $entity) {
      $bundles = $this->bundleInfo->getBundleInfo($entity->id());

      if (!$entity->entityClassImplements(FieldableEntityInterface::class)) {
        continue;
      }

      foreach ($bundles as $key => $bundle) {
        $options[$entity->id() . ':' . $key] = (string) $bundle['label'];
      }
    }

    $query = $this->getRequest()->query;

    $bundle = $query->get('bundle');

    $form['bundle'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Entity bundle'),
      '#options'       => $options,
      '#default_value' => $bundle,
      '#empty_value'   => '',
    ];

    $type = $query->get('type');

    $form['type'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Type'),
      '#options'       => [
        'storage'             => $this->t('Field storage'),
        'instance'            => $this->t('Field instance'),
        'entity_form_display' => $this->t('Form display'),
        'entity_view_display' => $this->t('View display'),
      ],
      '#default_value' => $type,
      '#empty_value'   => '',
    ];

    $form['show'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Show'),
    ];

    if ($bundle) {
      try {
        $config = $this->getBundleConfig($bundle, $type);
      }
      catch (\Exception $e) {
        $this->messenger()->addError($e->getMessage());
        return $form;
      }

      $form['table'] = $this->buildTable($config);

      $form['folder'] = [
        '#type'     => 'textfield',
        '#title'    => $this->t('Export folder'),
        '#required' => FALSE,
      ];

      $form['export'] = [
        '#type'  => 'submit',
        '#value' => $this->t('Export'),
      ];

      $form['download'] = [
        '#type'  => 'submit',
        '#value' => $this->t('Download zip'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $triggering_element = $form_state->getTriggeringElement();

    $action = $triggering_element['#parents'][0];

    $bundle = $form_state->getValue('bundle');
    $type = $form_state->getValue('type');

    if ($action === 'show') {

      $query = ['bundle' => $bundle];
      if ($type) {
        $query['type'] = $type;
      }

      $url = Url::fromRoute('config.export_entity')
        ->setOption('query', $query);
      $form_state->setRedirectUrl($url);
    }
    elseif ($action === 'download') {
      $configs = array_filter(array_values($form_state->getValue('table')));
      $file_name = $bundle . ($type ? ".$type" : '');
      $form_state->setResponse($this->downloadZip($configs, $file_name));
    }
    else {
      parent::submitForm($form, $form_state);
    }
  }

  /**
   * Get config for this bundle.
   *
   * @param string $bundle
   *   The entity and bundle id seperated by :.
   * @param string|null $type
   *   The config type.
   *
   * @return array
   *   The configs.
   */
  public function getBundleConfig(string $bundle, ?string $type): array {
    [$entity_type, $bundle] = explode(':', $bundle);

    $configs = [];

    if (!$type || $type === 'entity_form_display') {
      $configs[] = "core.entity_form_display.$entity_type.$bundle.default";
    }

    if (!$type || $type === 'entity_view_display') {
      $configs[] = "core.entity_view_display.$entity_type.$bundle.default";
    }

    $fields = $this->fieldManager->getFieldDefinitions($entity_type, $bundle);

    foreach ($fields as $field) {
      if ($field instanceof FieldConfigInterface) {

        if (!$type || $type === 'storage') {
          $configs[] = 'field.storage.' . $entity_type . '.' . $field->getName();
        }

        if (!$type || $type === 'instance') {
          $configs[] = 'field.field.' . $field->id();
        }

      }
    }

    return $configs;

  }

}
