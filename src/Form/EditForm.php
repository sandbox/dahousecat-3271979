<?php

namespace Drupal\config_export_multiple\Form;

use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class to view a single config item.
 *
 * @package Drupal\config_export_multiple\Form
 */
class EditForm extends FormBase {

  /**
   * The config storage.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected StorageInterface $configStorage;

  /**
   * Constructs a new ConfigSingleImportForm.
   *
   * @param \Drupal\Core\Config\StorageInterface $configStorage
   *   The config storage.
   */
  public function __construct(StorageInterface $configStorage) {
    $this->configStorage = $configStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): EditForm {

    /** @var \Drupal\Core\Config\StorageInterface $config_storage */
    $config_storage = $container->get('config.storage');

    return new static($config_storage);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'view-single-config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $name = $this->getRequest()->get('name');

    $config = Yaml::encode($this->configStorage->read($name));

    $form['name'] = [
      '#title' => $this->t('Name'),
      '#type' => 'textfield',
      '#default_value' => $name,
      '#disabled' => TRUE,
    ];

    $form['config'] = [
      '#type' => 'textarea',
      '#rows' => 20,
      '#default_value' => $config,
    ];

    $form['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    $form['download'] = [
      '#type' => 'submit',
      '#value' => $this->t('Download'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $name = $this->getRequest()->get('name');

    $triggering_element = $form_state->getTriggeringElement();

    $action = $triggering_element['#parents'][0];

    if ($action === 'download') {
      $download = Url::fromRoute('config.download_single', ['name' => $name]);
      $form_state->setRedirectUrl($download);
      return;
    }

    $config = $this->configFactory()->getEditable($name);

    $data = Yaml::decode($form_state->getValue('config'));

    $config->setData($data);
    $config->save();

    $args = ['%name' => $name];
    $this->messenger()->addStatus($this->t('Updated %name', $args));

  }

}
