<?php

namespace Drupal\config_export_multiple\Controller;

use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Serialization\Yaml;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Config Export Multiple Controller.
 *
 * @package Drupal\config_export_multiple\Controller
 */
class ConfigExportMultipleController extends ControllerBase {

  /**
   * The config storage.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected StorageInterface $configStorage;

  /**
   * Constructs a new ConfigSingleImportForm.
   *
   * @param \Drupal\Core\Config\StorageInterface $configStorage
   *   The config storage.
   */
  public function __construct(StorageInterface $configStorage) {
    $this->configStorage = $configStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ConfigExportMultipleController {

    /** @var \Drupal\Core\Config\StorageInterface $config_storage */
    $config_storage = $container->get('config.storage');

    return new static($config_storage);
  }

  /**
   * Download a single config item.
   *
   * @param string $name
   *   The config name.
   *
   * @return \Symfony\Component\HttpFoundation\StreamedResponse
   *   The config download.
   */
  public function download(string $name): StreamedResponse {

    $config = Yaml::encode($this->configStorage->read($name));

    $response = new StreamedResponse();
    $response->setCallback(function () use ($config) {
      echo $config;
    });

    $filename = $name . '.yml';
    $response->setStatusCode(200);
    $response->headers->set('Content-Type', 'text/yaml; charset=utf-8');
    $response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '"');

    return $response;

  }

  /**
   * Delete a config item.
   *
   * @param string $name
   *   The config name.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response.
   */
  public function delete(string $name): RedirectResponse {

    $args = ['@name' => $name];

    if ($this->configStorage->delete($name)) {
      $message = $this->t('Deleted config @name', $args);
      $this->messenger()->addStatus($message);
    }
    else {
      $message = $this->t('Failed to delete config @name', $args);
      $this->messenger()->addError($message);
    }

    return $this->redirect('config.export_multiple');

  }

}
